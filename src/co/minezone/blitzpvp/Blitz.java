package co.minezone.blitzpvp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import co.minezone.blitzpvp.listeners.BlockListener;
import co.minezone.blitzpvp.listeners.PlayerListener;
import co.minezone.blitzpvp.listeners.ServerListener;

/**
 * 
 * This class manages the plugin.
 * @author Alex (DukerHD)
 *
 */
public class Blitz extends JavaPlugin {

	private static Blitz instance;

	private BlitzManager bzm;
	
	public ScoreboardManager manager;
	public Scoreboard board;

	public void onEnable() {
		instance = this;
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		board.registerNewTeam("Red");
		board.registerNewTeam("Blue");
		board.registerNewObjective("lobby", "dummy");
		board.registerNewObjective("pregame", "dummy");
		board.registerNewObjective("game", "dummy");
		bzm = BlitzManager.getInstance();
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new ServerListener(), this);
		saveDefaultConfig();
		bzm.GAMESTATE = bzm.LOBBY;
		bzm.update();

		for (World w : Bukkit.getWorlds()) {
			w.setMonsterSpawnLimit(0);
			w.setAnimalSpawnLimit(0);
			w.setAmbientSpawnLimit(0);
		}

		for (World w : getWorlds()) {
			Bukkit.createWorld(new WorldCreator(w.getName()));
			w.setTime(6000);
			w.setStorm(false);
			new ArenaManager().createWorldBackups(w.getName(), false);
		}

		setRandomWorld();
	}

	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
	}

	public void allWorldsTime() {
		for (World w : getWorlds()) {
			w.setTime(6000);
		}
		Bukkit.getWorlds().get(0).setTime(6000);
	}

	public static Blitz getInstance() {
		return instance;
	}
	
	public String getPrefix() {
		return ChatColor.YELLOW + "➟ ";
	}

	public HashMap<Player, String> core = new HashMap<Player, String>();

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		final Player p = (Player) sender;
		if (label.equalsIgnoreCase("gamestart")) {
			if (p.hasPermission("blitz.admin.start")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE == bzm.LOBBY) {
						bzm.gameStart();
					} else {
						p.sendMessage(getPrefix() + "§cYou can only do this during the lobby stage.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /gamestart");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("gamestop")) {
			if (p.hasPermission("blitz.admin.stop")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE == bzm.GAME) {
						bzm.endGame(false, null);
					} else {
						p.sendMessage(getPrefix() + "§cYou can only do this during the in-game stage.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /gamestop");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("adminmode")) {
			if (p.hasPermission("blitz.admin.mode")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE != bzm.ADMIN) {
						setAdminMode(true);
						for (final Player p2 : Bukkit.getOnlinePlayers()) {
							p2.sendMessage(getPrefix() + "§bAdmin mode enabled.");
							p2.sendMessage(getPrefix() + "§bServer restarting in §35 §bseconds.");
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §34 §bseconds.");
										}
									}, 1 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §33 §bseconds.");
										}
									}, 2 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §32 §bseconds.");
										}
									}, 3 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §31 §bsecond.");
										}
									}, 4 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§cServer is restarting...");
											try {
												ByteArrayOutputStream b = new ByteArrayOutputStream();
												DataOutputStream out = new DataOutputStream(
														b);
												out.writeUTF("Connect");
												out.writeUTF("hub");
												p2.sendPluginMessage(instance,
														"BungeeCord",
														b.toByteArray());
											} catch (Exception ex) {
												System.out
														.println("Unable to send player to hub/lobby server.");
												p2.kickPlayer(getPrefix() + "§cServer is restarting...");
											}
										}
									}, 5 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											Bukkit.shutdown();
										}
									}, 6 * 20L);
						}
					} else {
						setAdminMode(false);
						for (final Player p2 : Bukkit.getOnlinePlayers()) {
							p2.sendMessage(getPrefix() + "§bAdmin mode disabled.");
							p2.sendMessage(getPrefix() + "§bServer restarting in §35 §bseconds.");
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §34 §bseconds.");
										}
									}, 1 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §33 §bseconds.");
										}
									}, 2 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §32 §bseconds.");
										}
									}, 3 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§bServer restarting in §31 §bsecond.");
										}
									}, 4 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											p2.sendMessage(getPrefix() + "§cServer is restarting...");
											try {
												ByteArrayOutputStream b = new ByteArrayOutputStream();
												DataOutputStream out = new DataOutputStream(
														b);
												out.writeUTF("Connect");
												out.writeUTF("hub");
												p2.sendPluginMessage(instance,
														"BungeeCord",
														b.toByteArray());
											} catch (Exception ex) {
												System.out
														.println("Unable to send player to hub/lobby server.");
												p2.kickPlayer(getPrefix() + "§cServer is restarting...");
											}
										}
									}, 5 * 20L);
							Bukkit.getScheduler().scheduleSyncDelayedTask(this,
									new Runnable() {
										public void run() {
											Bukkit.shutdown();
										}
									}, 6 * 20L);
						}
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /adminmode");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("addteamspawn")) {
			if (p.hasPermission("blitz.admin.addspawn")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 1) {
					if (bzm.GAMESTATE == bzm.ADMIN) {
						if (isWorld(p.getWorld())) {
							if (args[0].equalsIgnoreCase("blue")) {
								saveBlueSpawn(p.getLocation());
								p.sendMessage(getPrefix() + "§bSuccessfully set §9Blue Team§b's spawn.");
							} else if (args[0].equalsIgnoreCase("red")) {
								saveRedSpawn(p.getLocation());
								p.sendMessage(getPrefix() + "§bSuccessfully set §cRed Team§b's spawn.");
							} else {
								p.sendMessage(getPrefix() + "§cPlease use these teams: blue, red.");
							}
						} else {
							p.sendMessage(getPrefix() + "§cYour current world is not an arena.");
						}
					} else {
						p.sendMessage(getPrefix() + "§cThe server must be in admin mode.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /addteamspawn [blue|red]");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("setpoints")) {
			if (p.hasPermission("blitz.admin.setpoints")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 2) {
					if (bzm.GAMESTATE == bzm.GAME) {
						if (args[0].equalsIgnoreCase("red")) {
							if (isInteger(args[1])) {
								int points = Integer.parseInt(args[1]);
								if (points < 200) {
									bzm.red_score = points;
									bzm.updateScoreboardPoints();
									p.sendMessage(getPrefix() + "§bSuccessfully set §cRed Team§b's points to §3"
											+ args[1] + " §bpoints.");
								} else {
									p.sendMessage(getPrefix() + "§cThe points must be a number under 200.");
								}
							} else {
								p.sendMessage(getPrefix() + "§cThe points must be a number.");
							}
						} else if (args[0].equalsIgnoreCase("blue")) {
							if (isInteger(args[1])) {
								int points = Integer.parseInt(args[1]);
								if (points < 200) {
									bzm.blue_score = points;
									bzm.updateScoreboardPoints();
									p.sendMessage(getPrefix() + "§bSuccessfully set §9Blue Team§b's points to §3"
											+ args[1] + " §bpoints.");
								} else {
									p.sendMessage(getPrefix() + "§cThe points must be a number under 200.");
								}
							} else {
								p.sendMessage(getPrefix() + "§cThe points must be a number.");
							}
						} else {
							p.sendMessage(getPrefix() + "§cPlease use these teams: blue, red.");
						}
					} else {
						p.sendMessage(getPrefix() + "§cYou can only do this during the in-game stage.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /setpoints [blue|red] [points]");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("addplatformspawn")) {
			if (p.hasPermission("blitz.admin.addspawn")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE == bzm.ADMIN) {
						if (isWorld(p.getWorld())) {
							savePlatformSpawn(p.getLocation());
							p.sendMessage(getPrefix() + "§bSuccessfully set platform spawn.");
						} else {
							p.sendMessage(getPrefix() + "§cYour current world is not an arena.");
						}
					} else {
						p.sendMessage(getPrefix() + "§cThe server must be in admin mode.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /addplatformspawn");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("savemaps")) {
			if (p.hasPermission("blitz.admin.save")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE == bzm.ADMIN) {
						p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
						for (World w : getWorlds()) {
							new ArenaManager().createWorldBackups(w.getName(),
									true);
							new ArenaManager().resetArena(w.getName());
						}
						p.sendMessage(getPrefix() + "§bSuccessfully saved all maps.");
					} else {
						p.sendMessage(getPrefix() + "§cThe server must be in admin mode.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /addplatformspawn");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("blitzreload")) {
			if (p.hasPermission("blitz.admin.reload")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE == bzm.ADMIN) {
						reloadConfig();
						p.sendMessage(getPrefix() + "§bConfig reloaded.");
					} else {
						p.sendMessage(getPrefix() + "§cThe server must be in admin mode.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /blitzreload");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("addcore")) {
			if (p.hasPermission("blitz.admin.addcore")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 1) {
					if (bzm.GAMESTATE == bzm.ADMIN) {
						if (isWorld(p.getWorld())) {
							if (args[0].equalsIgnoreCase("red")) {
								core.put(p, "red");
								p.sendMessage(getPrefix() + "§bPunch a block to add it as §cRed Team§b's core.");
								p.sendMessage(getPrefix() + "§bUse §3/cancelcore §bto cancel.");
							} else if (args[0].equalsIgnoreCase("blue")) {
								core.put(p, "blue");
								p.sendMessage(getPrefix() + "§bPunch a block to add it as §9Blue Team§b's core.");
								p.sendMessage(getPrefix() + "§bUse §3/cancelcore §bto cancel.");
							} else {
								p.sendMessage(getPrefix() + "§cPlease use these teams: blue, red.");
							}
						} else {
							p.sendMessage(getPrefix() + "§cYour current world is not an arena.");
						}
					} else {
						p.sendMessage(getPrefix() + "§cThe server must be in admin mode.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /addcore [blue|red]");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		} else if (label.equalsIgnoreCase("cancelcore")) {
			if (p.hasPermission("blitz.admin.addcore")
					|| p.hasPermission("blitz.admin.*")) {
				if (args.length == 0) {
					if (bzm.GAMESTATE == bzm.ADMIN) {
						if (core.containsKey(p)) {
							core.remove(p);
							p.sendMessage(getPrefix() + "§bSuccessfully cancelled §3/addcore");
						} else if (!core.containsKey(p)) {
							p.sendMessage(getPrefix() + "§cYou did not run the /addcore command.");
						}
					} else {
						p.sendMessage(getPrefix() + "§cThe server must be in admin mode.");
					}
				} else {
					p.sendMessage(getPrefix() + "§cUsage: /cancelcore");
				}
			} else {
				p.sendMessage(getPrefix() + "§4Insufficient rank.");
			}
		}
		return false;
	}

	public List<World> getWorlds() {
		ArrayList<String> list = new ArrayList<String>(getConfig()
				.getConfigurationSection("worlds").getKeys(false));
		String s = list.toString().replace("[", "").replace("]", "");
		String[] s1 = s.split(", ");
		List<String> worlds = new ArrayList<String>(Arrays.asList(s1));
		List<World> worldlist = new ArrayList<World>();
		for (String w : worlds) {
			worldlist.add(Bukkit.getWorld(w));
		}
		return worldlist;
	}

	public World getRandomWorld() {
		Random r = new Random();
		int size = getWorlds().size();
		int world = r.nextInt(size);
		World w = getWorlds().get(world);
		System.out.println(w.getName()
				+ " is the random world picked by BlitzPvP.");
		return w;
	}

	public void setRandomWorld() {
		World world1 = getRandomWorld();
		final World world = world1;
		bzm.current_world = world;
	}

	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c <= '/' || c >= ':') {
				return false;
			}
		}
		return true;
	}

	public boolean isWorld(World w) {
		for (String s : getConfig().getConfigurationSection("worlds").getKeys(
				false)) {
			if (s.equalsIgnoreCase(w.getName())) {
				return true;
			}
		}
		return false;
	}

	public Double fiveDec(double val) {
		DecimalFormat df5 = new DecimalFormat("#####.#####");
		return Double.valueOf(df5.format(val));
	}

	public Double threeDec(double val) {
		DecimalFormat df3 = new DecimalFormat("#####.###");
		return Double.valueOf(df3.format(val));
	}

	public Double oneDec(double val) {
		DecimalFormat df1 = new DecimalFormat("#####.#");
		return Double.valueOf(df1.format(val));
	}

	public void saveBlueSpawn(Location loc) {
		getConfig().set(
				"worlds." + loc.getWorld().getName() + ".spawns.blue",
				fiveDec(loc.getX()) + "," + threeDec(loc.getY()) + ","
						+ fiveDec(loc.getZ()) + "," + fiveDec(loc.getYaw())
						+ "," + fiveDec(loc.getPitch()));
		saveConfig();
	}

	public void saveRedSpawn(Location loc) {
		getConfig().set(
				"worlds." + loc.getWorld().getName() + ".spawns.red",
				fiveDec(loc.getX()) + "," + threeDec(loc.getY()) + ","
						+ fiveDec(loc.getZ()) + "," + fiveDec(loc.getYaw())
						+ "," + fiveDec(loc.getPitch()));
		saveConfig();
	}

	public void savePlatformSpawn(Location loc) {
		getConfig().set(
				"worlds." + loc.getWorld().getName() + ".lobby-spawn",
				fiveDec(loc.getX()) + "," + threeDec(loc.getY()) + ","
						+ fiveDec(loc.getZ()) + "," + fiveDec(loc.getYaw())
						+ "," + fiveDec(loc.getPitch()));
		saveConfig();
	}

	public void addCoreSpawn(Location loc, String team) {
		getConfig().set(
				"worlds." + loc.getWorld().getName() + ".cores." + team,
				oneDec(loc.getX()) + "," + oneDec(loc.getY()) + ","
						+ oneDec(loc.getZ()));
		saveConfig();
	}

	public Location getTeamSpawn(World world, String team) {
		String s1 = getConfig().getString(
				"worlds." + world.getName() + ".spawns." + team);
		String[] s = s1.split(",");
		double x = Double.parseDouble(s[0]);
		double y = Double.parseDouble(s[1]);
		double z = Double.parseDouble(s[2]);
		float yaw = Float.parseFloat(s[3]);
		float pitch = Float.parseFloat(s[4]);
		Location loc = new Location(world, x, y, z, yaw, pitch);
		return loc;
	}

	public Location getPlatformSpawn(World world) {
		String s1 = getConfig().getString(
				"worlds." + world.getName() + ".lobby-spawn");
		String[] s = s1.split(",");
		double x = Double.parseDouble(s[0]);
		double y = Double.parseDouble(s[1]);
		double z = Double.parseDouble(s[2]);
		float yaw = Float.parseFloat(s[3]);
		float pitch = Float.parseFloat(s[4]);
		Location loc = new Location(world, x, y, z, yaw, pitch);
		return loc;
	}

	public String getWorldName(World w) {
		String world = "";
		if (getConfig().getString("worlds." + w.getName() + ".name") != null) {
			world = ChatColor.translateAlternateColorCodes('&', getConfig()
					.getString("worlds." + w.getName() + ".name"));
		}
		return world;
	}

	public String getTeamColor(Player p) {
		if (bzm.red.hasPlayer(p))
			return bzm.red.getPrefix();
		if (bzm.blue.hasPlayer(p))
			return bzm.blue.getPrefix();
		return "";
	}

	public void setAdminMode(boolean admin) {
		getConfig().set("admin-mode", admin);
		saveConfig();
	}

	public boolean isAdminMode() {
		if (getConfig().getBoolean("admin-mode")) {
			return true;
		}
		return false;
	}

}
