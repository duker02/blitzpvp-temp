package co.minezone.blitzpvp;

public class TimeUtils {
	
    public static String getTime(int seconds) {
        return getTime(seconds, ":", ":", ":", ":", ":", ":");
    }

    public static String getFullTime(int seconds) {
        return getTime(seconds, " seconds", " minutes", " hours", " days", " months", " years");
    }

    public static String getTime(int seconds, String secStr, String minStr, 
            String hourStr, String dayStr, String monthStr, String yearStr) {
        String time = "";
        int MINUTE = 60, HOUR = 60*MINUTE, DAY = 24*HOUR, MONTH = 30*DAY, YEAR = 12*MONTH;
        int secs = seconds;
        if (secs >= YEAR) {
            int years = (int) (secs / YEAR);
            secs -= years * YEAR;
            time = time + years + yearStr;
        }
        if (secs >= MONTH) {
            int months = (int) (secs / MONTH);
            secs -= months * MONTH;
            time = time + months + monthStr;
        }
        if (secs >= DAY) {
            int days = (int) (secs / DAY);
            secs -= days * DAY;
            time = time + days + dayStr;
        }
        if (secs >= HOUR) {
            int hours = (int) (secs / HOUR);
            secs -= hours * HOUR;
            time = time + hours + hourStr;
        }
        if (secs > MINUTE) {
            int minutes = (int) (secs / MINUTE);
            secs -= minutes * MINUTE;
            time = time + minutes + minStr;
        }
        if(secs > 0 || time.equals(""))
            time = time + secs + secStr;

        return time;
    }

}
