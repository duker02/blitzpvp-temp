package co.minezone.blitzpvp.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.minezone.blitzpvp.Blitz;
import co.minezone.blitzpvp.BlitzManager;
import co.minezone.core.tokens.TokenAPI;

/**
 * 
 * This class manages the player events.
 * @author Alex (DukerHD)
 *
 */
public class PlayerListener implements Listener {

	private Blitz blitz = Blitz.getInstance();

	private BlitzManager bzm = BlitzManager.getInstance();

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent ev) {
		if (bzm.GAMESTATE == bzm.ADMIN || bzm.GAMESTATE == bzm.ENDING
				|| bzm.GAMESTATE == bzm.LOBBY) {
			ev.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerPVP(EntityDamageByEntityEvent ev) {
		Entity ent = ev.getEntity();
		Entity dmg = ev.getDamager();
		if (bzm.GAMESTATE != bzm.GAME && bzm.GAMESTATE != bzm.ADMIN) {
			ev.setCancelled(true);
		}
		if (ent instanceof Player && dmg instanceof Player) {
			Player p = (Player) ent;
			Player d = (Player) dmg;
			if (bzm.red.hasPlayer(p) && bzm.red.hasPlayer(d))
				ev.setCancelled(true);
			if (bzm.blue.hasPlayer(p) && bzm.blue.hasPlayer(d))
				ev.setCancelled(true);
			if (bzm.invinc.contains(p))
				ev.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent ev) {
		Player p = ev.getPlayer();
		if (bzm.GAMESTATE == bzm.LOBBY) {
			if (p.getLocation().getY() < -4.0D) {
				p.teleport(blitz.getPlatformSpawn(bzm.current_world));
				p.setAllowFlight(true);
				p.setFlying(true);
			}
		}
	}

	@EventHandler
	public void onPlayerDamage(EntityDamageEvent ev) {
		Entity ent = ev.getEntity();
		if (ent instanceof Player) {
			if (bzm.invinc.contains((Player) ent)) {
				ev.setCancelled(true);
			}
			if (bzm.GAMESTATE != bzm.GAME && bzm.GAMESTATE != bzm.ADMIN) {
				ev.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent ev) {
		ev.getDrops().clear();
		Entity ent = ev.getEntity();
		if (ent instanceof Player) {
			if (bzm.GAMESTATE == bzm.GAME) {
				Player p = (Player) ent;
				Player k = (Player) p.getKiller();
				String prefix = "";
				EntityDamageEvent dc = p.getLastDamageCause();
				if (dc instanceof EntityDamageByEntityEvent
						&& dc.getCause() == DamageCause.ENTITY_ATTACK) {
					if (k instanceof Player
							&& ((EntityDamageByEntityEvent) dc).getDamager() instanceof Player) {
						ev.setDeathMessage(prefix + blitz.getTeamColor(p)
								+ p.getDisplayName() + " �7was slain by "
								+ blitz.getTeamColor(k) + k.getDisplayName());
						TokenAPI.giveTokens(k, 5);
						k.sendMessage("�bYou've received �35 �btokens for killing "
								+ blitz.getTeamColor(p) + p.getDisplayName());
						if (bzm.red.hasPlayer(k)) {
							if (bzm.red_score >= 199) {
								bzm.addRedPoint();
								bzm.endGame(true, "red");
							} else if (bzm.red_score < 199) {
								bzm.addRedPoint();
							}
						}
						if (bzm.blue.hasPlayer(k)) {
							if (bzm.blue_score >= 199) {
								bzm.addBluePoint();
								bzm.endGame(true, "blue");
							} else if (bzm.blue_score < 199) {
								bzm.addBluePoint();
							}
						}
					} else {
						ev.setDeathMessage(null);
					}
				} else if (dc instanceof EntityDamageByEntityEvent
						&& dc.getCause() == DamageCause.PROJECTILE) {
					if (dc instanceof EntityDamageByEntityEvent) {
						Projectile proj = (Projectile) ((EntityDamageByEntityEvent) dc)
								.getDamager();
						LivingEntity shooter = (LivingEntity) proj.getShooter();
						if (shooter instanceof Player) {
							Player s = (Player) shooter;
							ev.setDeathMessage(prefix + blitz.getTeamColor(p)
									+ p.getDisplayName() + " �7was shot by "
									+ blitz.getTeamColor(s)
									+ s.getDisplayName());
							TokenAPI.giveTokens(s, 5);
							s.sendMessage("�bYou've received �35 �btokens for killing "
									+ blitz.getTeamColor(p)
									+ p.getDisplayName());
							if (bzm.red.hasPlayer(s)) {
								if (bzm.red_score >= 199) {
									bzm.addRedPoint();
									bzm.endGame(true, "red");
								} else if (bzm.red_score < 199) {
									bzm.addRedPoint();
								}
							}
							if (bzm.blue.hasPlayer(s)) {
								if (bzm.blue_score >= 199) {
									bzm.addBluePoint();
									bzm.endGame(true, "blue");
								} else if (bzm.blue_score < 199) {
									bzm.addBluePoint();
								}
							}
						}
					}
				} else if (dc.getCause() == DamageCause.BLOCK_EXPLOSION) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " �7has blown up.");
					if (bzm.blue.hasPlayer(p)) {
						if (bzm.red_score >= 199) {
							bzm.addRedPoint();
							bzm.endGame(true, "red");
						} else if (bzm.red_score < 199) {
							bzm.addRedPoint();
						}
					}
					if (bzm.red.hasPlayer(p)) {
						if (bzm.blue_score >= 199) {
							bzm.addBluePoint();
							bzm.endGame(true, "blue");
						} else if (bzm.blue_score < 199) {
							bzm.addBluePoint();
						}
					}
				} else if (dc.getCause() == DamageCause.DROWNING) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " �7has drowned.");
				} else if (dc.getCause() == DamageCause.ENTITY_EXPLOSION) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " �7has blown up.");
					if (bzm.blue.hasPlayer(p)) {
						if (bzm.red_score >= 199) {
							bzm.addRedPoint();
							bzm.endGame(true, "red");
						} else if (bzm.red_score < 199) {
							bzm.addRedPoint();
						}
					}
					if (bzm.red.hasPlayer(p)) {
						if (bzm.blue_score >= 199) {
							bzm.addBluePoint();
							bzm.endGame(true, "blue");
						} else if (bzm.blue_score < 199) {
							bzm.addBluePoint();
						}
					}
				} else if (dc.getCause() == DamageCause.FALL) {
					if (k instanceof Player) {
						ev.setDeathMessage(prefix + blitz.getTeamColor(p)
								+ p.getDisplayName() + ChatColor.GRAY
								+ " was pushed off by " + blitz.getTeamColor(k)
								+ k.getDisplayName());
						TokenAPI.giveTokens(k, 5);
						k.sendMessage("�bYou've received �35 �btokens for killing "
								+ blitz.getTeamColor(p) + p.getDisplayName());
						if (bzm.blue.hasPlayer(p)) {
							if (bzm.red_score >= 199) {
								bzm.addRedPoint();
								bzm.endGame(true, "red");
							} else if (bzm.red_score < 199) {
								bzm.addRedPoint();
							}
						}
						if (bzm.red.hasPlayer(p)) {
							if (bzm.blue_score >= 199) {
								bzm.addBluePoint();
								bzm.endGame(true, "blue");
							} else if (bzm.blue_score < 199) {
								bzm.addBluePoint();
							}
						}
					} else {
						ev.setDeathMessage(prefix + blitz.getTeamColor(p)
								+ p.getDisplayName() + ChatColor.GRAY
								+ " fell to their death.");
					}
				} else if (dc.getCause() == DamageCause.FALLING_BLOCK) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " has been squashed.");
				} else if (dc.getCause() == DamageCause.FIRE) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " has burnt to death.");
					if (bzm.blue.hasPlayer(p)) {
						if (bzm.red_score >= 199) {
							bzm.addRedPoint();
							bzm.endGame(true, "red");
						} else if (bzm.red_score < 199) {
							bzm.addRedPoint();
						}
					}
					if (bzm.red.hasPlayer(p)) {
						if (bzm.blue_score >= 199) {
							bzm.addBluePoint();
							bzm.endGame(true, "blue");
						} else if (bzm.blue_score < 199) {
							bzm.addBluePoint();
						}
					}
				} else if (dc.getCause() == DamageCause.FIRE_TICK) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " has burnt to death.");
					if (bzm.blue.hasPlayer(p)) {
						if (bzm.red_score >= 199) {
							bzm.addRedPoint();
							bzm.endGame(true, "red");
						} else if (bzm.red_score < 199) {
							bzm.addRedPoint();
						}
					}
					if (bzm.red.hasPlayer(p)) {
						if (bzm.blue_score >= 199) {
							bzm.addBluePoint();
							bzm.endGame(true, "blue");
						} else if (bzm.blue_score < 199) {
							bzm.addBluePoint();
						}
					}
				} else if (dc.getCause() == DamageCause.LAVA) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " has burnt to death.");
					if (bzm.blue.hasPlayer(p)) {
						if (bzm.red_score >= 199) {
							bzm.addRedPoint();
							bzm.endGame(true, "red");
						} else if (bzm.red_score < 199) {
							bzm.addRedPoint();
						}
					}
					if (bzm.red.hasPlayer(p)) {
						if (bzm.blue_score >= 199) {
							bzm.addBluePoint();
							bzm.endGame(true, "blue");
						} else if (bzm.blue_score < 199) {
							bzm.addBluePoint();
						}
					}
				} else if (dc.getCause() == DamageCause.LIGHTNING) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " was struck to death.");
				} else if (dc.getCause() == DamageCause.POISON) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " was poisoned to death.");
					if (bzm.blue.hasPlayer(p)) {
						if (bzm.red_score >= 199) {
							bzm.addRedPoint();
							bzm.endGame(true, "red");
						} else if (bzm.red_score < 199) {
							bzm.addRedPoint();
						}
					}
					if (bzm.red.hasPlayer(p)) {
						if (bzm.blue_score >= 199) {
							bzm.addBluePoint();
							bzm.endGame(true, "blue");
						} else if (bzm.blue_score < 199) {
							bzm.addBluePoint();
						}
					}
				} else if (dc.getCause() == DamageCause.STARVATION) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " starved to death.");
				} else if (dc.getCause() == DamageCause.SUFFOCATION) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " suffocated.");
				} else if (dc.getCause() == DamageCause.SUICIDE) {
					ev.setDeathMessage(prefix + blitz.getTeamColor(p)
							+ p.getDisplayName() + ChatColor.GRAY
							+ " committed suicide.");
				} else if (dc.getCause() == DamageCause.VOID) {
					if (k instanceof Player) {
						ev.setDeathMessage(prefix + blitz.getTeamColor(p)
								+ p.getDisplayName() + ChatColor.GRAY
								+ " was pushed off by " + blitz.getTeamColor(k)
								+ k.getDisplayName());
						TokenAPI.giveTokens(k, 5);
						k.sendMessage("�bYou've received �35 �btokens for killing "
								+ blitz.getTeamColor(p) + p.getDisplayName());
						if (bzm.blue.hasPlayer(p)) {
							if (bzm.red_score >= 199) {
								bzm.addRedPoint();
								bzm.endGame(true, "red");
							} else if (bzm.red_score < 199) {
								bzm.addRedPoint();
							}
						}
						if (bzm.red.hasPlayer(p)) {
							if (bzm.blue_score >= 199) {
								bzm.addBluePoint();
								bzm.endGame(true, "blue");
							} else if (bzm.blue_score < 199) {
								bzm.addBluePoint();
							}
						}
					} else {
						ev.setDeathMessage(prefix + blitz.getTeamColor(p)
								+ p.getDisplayName() + ChatColor.GRAY
								+ " fell into the void.");
					}
				} else {
					ev.setDeathMessage(null);
				}
			} else {
				ev.setDeathMessage(null);
			}
		}
	}

	@EventHandler
	public void onPlayerRespawn(final PlayerRespawnEvent ev) {
		if (bzm.GAMESTATE == bzm.GAME || bzm.GAMESTATE == bzm.ENDING) {
			if (bzm.blue.hasPlayer(ev.getPlayer()))
				ev.setRespawnLocation(blitz.getTeamSpawn(bzm.current_world,
						"blue"));
			if (bzm.red.hasPlayer(ev.getPlayer()))
				ev.setRespawnLocation(blitz.getTeamSpawn(bzm.current_world,
						"red"));
			if (bzm.GAMESTATE == bzm.GAME) {
				bzm.giveKit(ev.getPlayer());
				bzm.invinc.add(ev.getPlayer());
				Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
						new Runnable() {
							public void run() {
								bzm.invinc.remove(ev.getPlayer());
							}
						}, 3 * 20L);
			}
		} else {
			ev.setRespawnLocation(Bukkit.getWorlds().get(0).getSpawnLocation());
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent ev) {
		Player p = ev.getPlayer();
		if (bzm.GAMESTATE == bzm.LOBBY) {
			ev.setFormat("�f" + p.getDisplayName() + "�7: �f" + ev.getMessage());
		} else if (bzm.GAMESTATE == bzm.GAME || bzm.GAMESTATE == bzm.ENDING) {
			if (bzm.red.hasPlayer(p))
				ev.setFormat("�c" + p.getDisplayName() + "�7: �f"
						+ ev.getMessage());
			if (bzm.blue.hasPlayer(p))
				ev.setFormat("�9" + p.getDisplayName() + "�7: �f"
						+ ev.getMessage());
		} else if (bzm.GAMESTATE == bzm.ADMIN) {
			ev.setFormat("�7[�cAdmin�7] �c" + p.getDisplayName() + "�7: �f"
					+ ev.getMessage());
		}
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent ev) {
		Player p = ev.getPlayer();
		if (bzm.GAMESTATE == bzm.LOBBY) {
			if (bzm.red.getSize() < bzm.blue.getSize()) {
				bzm.red.addPlayer(p);
			} else {
				bzm.blue.addPlayer(p);
			}
		} else if (bzm.GAMESTATE == bzm.GAME) {
			if (bzm.red.getSize() < bzm.blue.getSize()) {
				bzm.red.addPlayer(p);
			} else {
				bzm.blue.addPlayer(p);
			}
		} else if (bzm.GAMESTATE == bzm.ENDING) {
			ev.disallow(Result.KICK_OTHER, "The game is ending.");
		} else if (bzm.GAMESTATE == bzm.ADMIN) {
			if (p.hasPermission("blitz.admin.*")
					|| p.hasPermission("blitz.admin.login")) {
				ev.allow();
			} else {
				ev.disallow(Result.KICK_OTHER,
						blitz.getPrefix() + "�cThe server is in admin mode.");
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent ev) {
		Player p = ev.getPlayer();
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		if (bzm.GAMESTATE == bzm.LOBBY || bzm.GAMESTATE == bzm.ADMIN) {
			bzm.updateScoreboard();
			ev.setJoinMessage(blitz.getPrefix() + "�7" + p.getDisplayName()
					+ " �bhas joined �3BlitzPvP");
			p.teleport(blitz.getPlatformSpawn(bzm.current_world));
			p.setAllowFlight(true);
			p.setFlying(true);
			p.setHealth(20);
			p.setFoodLevel(20);
			p.sendMessage(blitz.getPrefix() + "�bYou can fly in the pre-game stage!");
		} else if (bzm.GAMESTATE == bzm.GAME) {
			ev.setJoinMessage(blitz.getPrefix() + "�7" + p.getDisplayName()
					+ " �bhas joined �3BlitzPvP");
			if (bzm.red.hasPlayer(p)) {
				p.teleport(blitz.getTeamSpawn(bzm.current_world, "red"));
			}
			if (bzm.blue.hasPlayer(p)) {
				p.teleport(blitz.getTeamSpawn(bzm.current_world, "blue"));
			}
			bzm.giveKit(p);
		} else if (bzm.GAMESTATE == bzm.ENDING) {
			ev.setJoinMessage(null);
			p.kickPlayer("The game has already started or is ending.");
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent ev) {
		Player p = ev.getPlayer();
		if (bzm.GAMESTATE == bzm.LOBBY || bzm.GAMESTATE == bzm.ADMIN
				|| bzm.GAMESTATE == bzm.GAME) {
			ev.setQuitMessage(blitz.getPrefix() + "�7" + p.getDisplayName()
					+ " �bhas left �3BlitzPvP");
		} else if (bzm.GAMESTATE == bzm.ENDING) {
			ev.setQuitMessage(null);
		}
		if (bzm.red.hasPlayer(p))
			bzm.red.removePlayer(p);
		if (bzm.blue.hasPlayer(p))
			bzm.blue.removePlayer(p);
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent ev) {
		Player p = ev.getPlayer();
		if (bzm.GAMESTATE == bzm.LOBBY || bzm.GAMESTATE == bzm.ADMIN
				|| bzm.GAMESTATE == bzm.GAME) {
			ev.setLeaveMessage(blitz.getPrefix() + "�7" + p.getDisplayName()
					+ " �bhas been kicked");
		} else if (bzm.GAMESTATE == bzm.ENDING) {
			ev.setLeaveMessage(null);
		}
		if (bzm.red.hasPlayer(p))
			bzm.red.removePlayer(p);
		if (bzm.blue.hasPlayer(p))
			bzm.blue.removePlayer(p);
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent ev) {
		if (bzm.GAMESTATE != bzm.ADMIN)
			ev.setCancelled(true);
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev) {
		Player p = ev.getPlayer();
		if (bzm.GAMESTATE != bzm.GAME || bzm.GAMESTATE != bzm.ADMIN) {
			ev.setCancelled(true);
		}
		if ((ev.getAction() == Action.LEFT_CLICK_BLOCK || ev.getAction() == Action.RIGHT_CLICK_BLOCK)
				&& blitz.core.containsKey(p)) {
			blitz.addCoreSpawn(ev.getClickedBlock().getLocation(),
					blitz.core.get(p));
			if (blitz.core.get(p).equalsIgnoreCase("red")) {
				p.sendMessage(blitz.getPrefix() + "�bSuccessfully set �cRed Team�b's core.");
			} else if (blitz.core.get(p).equalsIgnoreCase("blue")) {
				p.sendMessage(blitz.getPrefix() + "�bSuccessfully set �9Blue Team�b's core.");
			}
		}
		if ((ev.getAction() == Action.RIGHT_CLICK_BLOCK || ev.getAction() == Action.RIGHT_CLICK_AIR)
				&& p.getItemInHand().getType() == Material.NETHER_STAR) {
			bzm.openShop(p);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(InventoryClickEvent ev) {
		Player p = (Player) ev.getWhoClicked();
		ItemStack item = ev.getCurrentItem();
		Inventory inv = ev.getInventory();
		if (inv.getName().equals("�1BlitzPvP Shop")) {
			ev.setCancelled(true);
			if (item != null) {
				if (item.getType() == Material.STONE) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 25) {
						TokenAPI.removeTokens(p.getUniqueId(), 25);
						p.getInventory().addItem(
								new ItemStack(Material.STONE, 64));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.BREAD) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 10) {
						TokenAPI.removeTokens(p.getUniqueId(), 10);
						p.getInventory().addItem(
								new ItemStack(Material.BREAD, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.GOLDEN_APPLE
						&& item.getData().getData() == 0 && item.hasItemMeta()) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 50) {
						TokenAPI.removeTokens(p.getUniqueId(), 50);
						p.getInventory().addItem(
								new ItemStack(Material.GOLDEN_APPLE, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.GOLDEN_APPLE
						&& item.getData().getData() == 1 && item.hasItemMeta()) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 200) {
						TokenAPI.removeTokens(p.getUniqueId(), 200);
						p.getInventory().addItem(
								new ItemStack(Material.GOLDEN_APPLE, 1,
										(short) 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.DIAMOND_SWORD) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 250) {
						TokenAPI.removeTokens(p.getUniqueId(), 250);
						p.getInventory().addItem(
								new ItemStack(Material.DIAMOND_SWORD, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.DIAMOND_HELMET) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 150) {
						TokenAPI.removeTokens(p.getUniqueId(), 150);
						p.getInventory().addItem(
								new ItemStack(Material.DIAMOND_HELMET, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.DIAMOND_CHESTPLATE) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 150) {
						TokenAPI.removeTokens(p.getUniqueId(), 150);
						p.getInventory().addItem(
								new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.DIAMOND_LEGGINGS) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 150) {
						TokenAPI.removeTokens(p.getUniqueId(), 150);
						p.getInventory().addItem(
								new ItemStack(Material.DIAMOND_LEGGINGS, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.DIAMOND_BOOTS) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 150) {
						TokenAPI.removeTokens(p.getUniqueId(), 150);
						p.getInventory().addItem(
								new ItemStack(Material.DIAMOND_BOOTS, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.WATER) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 30) {
						TokenAPI.removeTokens(p.getUniqueId(), 30);
						p.getInventory().addItem(
								new ItemStack(Material.WATER, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.LAVA) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 40) {
						TokenAPI.removeTokens(p.getUniqueId(), 40);
						p.getInventory().addItem(
								new ItemStack(Material.LAVA, 1));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.MONSTER_EGG
						&& item.getData().getData() == 50 && item.hasItemMeta()) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 150) {
						TokenAPI.removeTokens(p.getUniqueId(), 150);
						p.getInventory().addItem(
								new ItemStack(Material.MONSTER_EGG, 1,
										(short) 50));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.POTION
						&& item.getItemMeta().getDisplayName()
								.equals("Splash Potion of Poison")
						&& item.hasItemMeta()) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 40) {
						TokenAPI.removeTokens(p.getUniqueId(), 40);
						ItemStack ps = new ItemStack(Material.POTION, 1,
								(short) 16388);
						PotionMeta psmeta = (PotionMeta) ps.getItemMeta();
						PotionEffect effect = new PotionEffect(
								PotionEffectType.POISON, 10 * 20, 0);
						psmeta.addCustomEffect(effect, true);
						psmeta.setDisplayName("Splash Potion of Poison");
						ps.setItemMeta(psmeta);
						p.getInventory().addItem(ps);
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount() + " �bof �3"
								+ item.getItemMeta().getDisplayName());
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				} else if (item.getType() == Material.POTION
						&& item.getItemMeta().getDisplayName()
								.equals("Splash Potion of Healing")
						&& item.hasItemMeta()) {
					p.closeInventory();
					if (TokenAPI.getTokens(p.getUniqueId()) >= 40) {
						TokenAPI.removeTokens(p.getUniqueId(), 40);
						p.getInventory()
								.addItem(
										new ItemStack(Material.POTION, 1,
												(short) 16421));
						p.sendMessage(blitz.getPrefix() + "�bYou have purchased �3"
								+ item.getAmount()
								+ " �bof �3"
								+ item.getType().toString().toLowerCase()
										.replace("_", " "));
						p.sendMessage(blitz.getPrefix() + "�bYour new balance: �3"
								+ TokenAPI.getTokens(p.getUniqueId()) + " �btokens");
						inv.setItem(22, bzm.getTokensItem(p));
					} else {
						p.sendMessage(blitz.getPrefix() + "�4Insufficient funds.");
					}
				}
			}
		}
	}
}
