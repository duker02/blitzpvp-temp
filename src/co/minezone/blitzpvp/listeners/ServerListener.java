package co.minezone.blitzpvp.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import co.minezone.blitzpvp.BlitzManager;

/**
 * 
 * This class manages the server events.
 * @author Alex (DukerHD)
 *
 */
public class ServerListener implements Listener {
	
	private BlitzManager bzm = BlitzManager.getInstance();
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent ev) {
		ev.setCancelled(true);
	}
	
	@EventHandler
	public void onServerListPingEvent(ServerListPingEvent ev) {
		String motd = "";
		if (bzm.GAMESTATE == bzm.LOBBY && Bukkit.getMaxPlayers() > Bukkit.getOnlinePlayers().length) {
			motd = ChatColor.GREEN + "" + ChatColor.BOLD + "[Join]" + "\n" + ChatColor.RESET + "Pre-Game";
		} else if (bzm.GAMESTATE == bzm.LOBBY && Bukkit.getMaxPlayers() <= Bukkit.getOnlinePlayers().length) {
			motd = ChatColor.DARK_RED + "" + ChatColor.BOLD + "[Full]" + "\n" + ChatColor.RESET + "Pre-Game";
		} else if (bzm.GAMESTATE == bzm.GAME) {
			motd = ChatColor.GREEN + "[Join]" + "\n" + ChatColor.RESET + "In-Game";
		} else if (bzm.GAMESTATE == bzm.ENDING) {
			motd = ChatColor.DARK_RED + "[NoJoin]" + "\n" + ChatColor.RESET + "Ending";
		} else if (bzm.GAMESTATE == bzm.ADMIN){
			motd = ChatColor.DARK_RED + "[NoJoin]" + "\n" + ChatColor.RESET + "Admins Only";
		}
		ev.setMotd(motd);
	}

}
