package co.minezone.blitzpvp.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import co.minezone.blitzpvp.Blitz;
import co.minezone.blitzpvp.BlitzManager;
import co.minezone.core.tokens.TokenAPI;

/**
 * 
 * This class manages the block events.
 * @author Alex (DukerHD)
 *
 */
public class BlockListener implements Listener {

	private Blitz blitz = Blitz.getInstance();

	private BlitzManager bzm = BlitzManager.getInstance();

	@EventHandler
	public void onBlockBreak(BlockBreakEvent ev) {
		if (blitz.core.containsKey(ev.getPlayer())) {
			ev.setCancelled(true);
		} else {
			if (bzm.GAMESTATE == bzm.GAME) {
				if (isCore(ev.getBlock().getLocation())) {
					if (isBlueCore(ev.getBlock().getLocation())) {
						if (bzm.red.hasPlayer(ev.getPlayer())) {
							TokenAPI.giveTokens(ev.getPlayer().getUniqueId(), 35);
							ev.getPlayer()
									.sendMessage(
											"�bYou've received �335 �btokens for breaking the core.");
							bzm.blue_core = false;
							bzm.core_regen.put(bzm.getBlueCore(ev.getPlayer().getWorld()), 300);
							ev.getBlock().getDrops().clear();
							bzm.red_score = bzm.red_score + 50;
							bzm.updateScoreboardPoints();
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "�bThe �9Blue Core �bhas been broken!");
							}
							if (bzm.red_score >= 200) {
								bzm.endGame(true, "red");
							}
						} else if (bzm.blue.hasPlayer(ev.getPlayer())) {
							ev.setCancelled(true);
							ev.getPlayer()
									.sendMessage(
											blitz.getPrefix() + "�cYou can't break your own team's core!");
						}
					}
					if (isRedCore(ev.getBlock().getLocation())) {
						if (bzm.blue.hasPlayer(ev.getPlayer())) {
							TokenAPI.giveTokens(ev.getPlayer().getUniqueId(), 35);
							ev.getPlayer()
									.sendMessage(
											"�bYou've received �335 �btokens for breaking the core.");
							bzm.red_core = false;
							ev.getBlock().getDrops().clear();
							bzm.blue_score = bzm.blue_score + 50;
							bzm.updateScoreboardPoints();
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "�bThe �cRed Core �bhas been broken!");
							}
							if (bzm.blue_score >= 200) {
								bzm.endGame(true, "blue");
							}
						} else if (bzm.red.hasPlayer(ev.getPlayer())) {
							ev.setCancelled(true);
							ev.getPlayer()
									.sendMessage(
											blitz.getPrefix() + "�cYou can't break your own team's core!");
						}
					}
				}
			} else if (bzm.GAMESTATE == bzm.LOBBY
					|| bzm.GAMESTATE == bzm.ENDING) {
				ev.setCancelled(true);
			} else if (bzm.GAMESTATE == bzm.ADMIN) {
				ev.setCancelled(false);
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent ev) {
		if (isCore(ev.getBlockPlaced().getLocation()) || (bzm.GAMESTATE == bzm.LOBBY || bzm.GAMESTATE == bzm.ENDING))
			ev.setCancelled(true);
	}

	public boolean isCore(Location loc) {
		String core = loc.getX() + "," + loc.getY() + "," + loc.getZ();
		// System.out.println("Is '" + core + "' a core location?");
		if (blitz
				.getConfig()
				.getString(
						"worlds." + bzm.current_world.getName() + ".cores.blue")
				.equals(core)) {
			// System.out.println("Yes.");
			return true;
		} else if (blitz
				.getConfig()
				.getString(
						"worlds." + bzm.current_world.getName() + ".cores.red")
				.equals(core)) {
			// System.out.println("Yes.");
			return true;
		}
		// System.out.println("No.");
		return false;
	}

	public boolean isBlueCore(Location loc) {
		String core = loc.getX() + "," + loc.getY() + "," + loc.getZ();
		// System.out.println("Is '" + core + "' the blue core's location?");
		if (blitz
				.getConfig()
				.getString(
						"worlds." + bzm.current_world.getName() + ".cores.blue")
				.equals(core)) {
			// System.out.println("Yes.");
			return true;
		}
		// System.out.println("No.");
		return false;
	}

	public boolean isRedCore(Location loc) {
		String core = loc.getX() + "," + loc.getY() + "," + loc.getZ();
		// System.out.println("Is '" + core + "' the red core's location?");
		if (blitz
				.getConfig()
				.getString(
						"worlds." + bzm.current_world.getName() + ".cores.red")
				.equals(core)) {
			// System.out.println("Yes.");
			return true;
		}
		// System.out.println("No.");
		return false;
	}
}
