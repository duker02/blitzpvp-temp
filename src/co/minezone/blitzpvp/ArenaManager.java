package co.minezone.blitzpvp;

import java.io.File;

import net.minecraft.util.org.apache.commons.io.FileUtils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * 
 * This class manages the arenas.
 * @author Alex (DukerHD)
 *
 */
public class ArenaManager {

	private Blitz blitz = Blitz.getInstance();
	
	public void resetArena(String wName) {
		final String fwName = wName;

		for (Player p : Bukkit.getWorld(fwName).getPlayers()) {
			p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		}
		
		new BukkitRunnable() {
			public void run() {
				resetWorld(fwName);
			}
		}.runTaskLater(blitz, 10L);
	}
	
	public void createWorldBackups(String worldName, boolean overwrite) {
		backupWorld(worldName, overwrite);
	}
	
    public void backupWorld(String worldName, boolean overwrite) {
    	File file = new File(blitz.getDataFolder().getAbsolutePath() + File.separator + worldName + "-backup");
    	if (!overwrite && file.exists()) {
    		System.out.println("    Using existing world backup '" + worldName + "'.");
    		return;
    	}
    	System.out.println("    Creating world backup '" + worldName + "-backup' in blitz folder.");
    	try {
    		long startTime = System.currentTimeMillis();
			File source = new File(blitz.getServer().getWorldContainer().getAbsolutePath() + File.separator + worldName);
			File dest = new File(blitz.getDataFolder().getAbsolutePath() + File.separator + worldName + "-backup");
			FileUtils.copyDirectory(source, dest);
			System.out.println("      Created world backup '" + worldName + "-backup' in " + (System.currentTimeMillis() - startTime) + "ms.");
		} catch (Exception e) {
			System.out.println("Unable to create backup for world '" + worldName + "'.");
			System.out.println(e.getMessage());
		}
    }
    
    public void resetWorld(String worldName) {
    	System.out.println("    Copying world backup '" + worldName + "-backup'...");
    	try {
    		long startTime = System.currentTimeMillis();
    		File source = new File(blitz.getDataFolder().getAbsolutePath() + File.separator + worldName + "-backup");
    		File dest = new File(blitz.getServer().getWorldContainer().getAbsolutePath() + File.separator + worldName);
    		FileUtils.copyDirectory(source, dest);
    		System.out.println("      Copied world backup '" + worldName + "-backup' in " + (System.currentTimeMillis() - startTime) + "ms.");
    	} catch (Exception e) {
    		System.out.println("Unable to copy backup for world '" + worldName + "'.");
			System.out.println(e.getMessage());
    	}
    }
	
}