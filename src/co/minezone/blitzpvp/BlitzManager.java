package co.minezone.blitzpvp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Team;

import co.minezone.core.tokens.TokenAPI;

/**
 * 
 * This class manages the game-mode.
 * @author Alex (DukerHD)
 *
 */
public class BlitzManager {

	private Blitz blitz = Blitz.getInstance();

	private static BlitzManager instance = new BlitzManager();

	public World current_world;

	public int GAMESTATE;
	public int LOBBY = 0;
	public int GAME = 1;
	public int ENDING = 2;
	public int ADMIN = 3;
	
	public int lobby_countdown = 60;
	
	public boolean countdown_started = false;
	
	public Team red = blitz.board.getTeam("Red");
	public Team blue = blitz.board.getTeam("Blue");
	
	public ArrayList<Player> invinc = new ArrayList<Player>();
	
	public HashMap<Location, Integer> core_regen = new HashMap<Location, Integer>();

	public int red_score = 0;
	public int blue_score = 0;

	public boolean blue_core = true;
	public boolean red_core = true;

	public static BlitzManager getInstance() {
		return instance;
	}

	public void update() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(blitz, new Runnable() {
			public void run() {
				blitz.allWorldsTime();
			}
		}, 0L, 20L);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(blitz, new Runnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				if (GAMESTATE != ADMIN) {
					for (Player p : Bukkit.getOnlinePlayers()) {
						if (p.getLocation()
								.distance(getBlueCore(current_world)) < 15) {
							if (!isBroken(getBlueCore(current_world)))
								p.playEffect(getBlueCore(current_world),
										Effect.ENDER_SIGNAL, 0);
						}
						if (p.getLocation().distance(getRedCore(current_world)) < 15) {
							if (!isBroken(getRedCore(current_world)))
								p.playEffect(getRedCore(current_world),
										Effect.ENDER_SIGNAL, 0);
						}
					}
				}
			}
		}, 0L, 20L);
		if (blitz.isAdminMode()) {
			GAMESTATE = ADMIN;
		} else {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(blitz, new Runnable() {
				public void run() {
					if (GAMESTATE == LOBBY) {
						for (Player p : Bukkit.getOnlinePlayers()) updateScoreboardLobby(p);
						if (Bukkit.getOnlinePlayers().length >= 10) {
							if (!countdown_started)
								lobbyCountdown();
						}
					} else if (GAMESTATE == GAME) {
						for (Player p : Bukkit.getOnlinePlayers()) setBar(p, red_core, blue_core);
					} else if (GAMESTATE != GAME) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							if (BarAPI.hasBar(p))
								BarAPI.removeBar(p);
						}
					}
				}
			}, 0L, 20L);
			Bukkit.getScheduler().scheduleSyncRepeatingTask(blitz,
					new Runnable() {
						public void run() {
							if (GAMESTATE == LOBBY) {
								if (Bukkit.getOnlinePlayers().length < 10) {
									for (Player p : Bukkit.getOnlinePlayers()) {
										int r = 10 - Bukkit.getOnlinePlayers().length;
										p.sendMessage(blitz.getPrefix() + "§bWaiting for §3"
												+ Integer.toString(r)
												+ "§b players to join.");
									}
								}
							}
						}
					}, 0L, 30 * 20L);
		}
	}

	public void lobbyCountdown() {
		countdown_started = true;
		if (GAMESTATE == LOBBY) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(blitz, new Runnable() {
				public void run() {
					lobby_countdown--;
				}
			}, 20L, 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								updateScoreboard();
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §31 §bminute.");
							}
						}
					}, 0 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §345 §bseconds.");
							}
						}
					}, 15 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §330 §bseconds.");
							}
						}
					}, 30 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §315 §bseconds.");
							}
						}
					}, 45 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §310 §bseconds.");
							}
						}
					}, 50 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §35 §bseconds.");
							}
						}
					}, 55 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §34 §bseconds.");
							}
						}
					}, 56 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §33 §bseconds.");
							}
						}
					}, 57 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §32 §bseconds.");
							}
						}
					}, 58 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(blitz.getPrefix() + "§bGame starting in §31 §bsecond.");
							}
						}
					}, 59 * 20L);
			Bukkit.getScheduler().scheduleSyncDelayedTask(blitz,
					new Runnable() {
						public void run() {
							gameStart();
						}
					}, 60 * 20L);
		}
	}

	public void gameStart() {
		GAMESTATE = GAME;
		for (Player p : Bukkit.getOnlinePlayers()) {
			red.setPrefix("§c");
			blue.setPrefix("§9");
			invinc.add(p);
			setBar(p, red_core, blue_core);
			p.setAllowFlight(false);
			p.setFlying(false);
			p.sendMessage(blitz.getPrefix() + "§bGame is starting! (" + getTeam(p)
					+ "§b)");
			p.sendMessage(blitz.getPrefix() + "§bYou are playing on §3"
					+ blitz.getWorldName(current_world));
			if (red.hasPlayer(p)) {
				p.teleport(blitz.getTeamSpawn(current_world, "red"));
				
			} else if (blue.hasPlayer(p)) {
				p.teleport(blitz.getTeamSpawn(current_world, "blue"));
				
			}
			updateScoreboard();
			p.getInventory().clear();
			giveKit(p);
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					invinc.remove(p);
				}
			}
		}, 3 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				if (GAMESTATE == GAME) {
					if (red_score < 200 && blue_score < 200) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							p.sendMessage(blitz.getPrefix() + "§bGame ends in §31 §bminute if no one wins.");
						}
					}
				}
			}
		}, 29 * 60 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				if (GAMESTATE == GAME) {
					if (red_score < 200 && blue_score < 200) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							p.sendMessage(blitz.getPrefix() + "§bGame is ending in §35 §bseconds if no one wins.");
						}
					}
				}
			}
		}, 35900L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				if (GAMESTATE == GAME) {
					if (red_score < 200 && blue_score < 200) {
						endGame(false, null);
					}
				}
			}
		}, 30 * 60 * 20L);
	}

	public void endGame(final boolean win, final String team) {
		GAMESTATE = ENDING;
		Bukkit.setWhitelist(true);
		final ArenaManager arenaman = new ArenaManager();
		for (Player p : Bukkit.getOnlinePlayers()) {
			BarAPI.removeBar(p);
			updateScoreboard();
			p.getInventory().clear();
			removeArmor(p);
			p.setAllowFlight(true);
			p.setFlying(true);
			p.sendMessage(blitz.getPrefix() + "§bGame has ended!");
			if (win) {
				if (team.equalsIgnoreCase("red")) {
					p.sendMessage(blitz.getPrefix() + "§bThe §cRed Team §bwins!");
					if (red.hasPlayer(p)) p.sendMessage("§bYou've received §330 §btokens for winning!");
				}
				if (team.equalsIgnoreCase("blue")) {
					p.sendMessage(blitz.getPrefix() + "§bThe §9Blue Team §bwins!");
					if (blue.hasPlayer(p)) p.sendMessage("§bYou've received §330 §btokens for winning!");
				}
			} else {
				p.sendMessage(blitz.getPrefix() + "§bThere are no winners!");
			}
		}
		Bukkit.getScheduler().scheduleSyncRepeatingTask(blitz, new Runnable() {
			public void run() {
				if (win) {
					if (team.equalsIgnoreCase("red")) {
						for (OfflinePlayer p2 : red.getPlayers()) {
							Player p1 = (Player) p2;
							Firework fw = (Firework) p1.getWorld().spawnEntity(
									p1.getLocation(), EntityType.FIREWORK);
							FireworkMeta fwm = fw.getFireworkMeta();
							Random r = new Random();
							int rt = r.nextInt(5) + 1;
							Type type = Type.BALL;
							if (rt == 1)
								type = Type.BALL;
							if (rt == 2)
								type = Type.BALL_LARGE;
							if (rt == 3)
								type = Type.BURST;
							if (rt == 4)
								type = Type.CREEPER;
							if (rt == 5)
								type = Type.STAR;

							int r1i = r.nextInt(17) + 1;
							int r2i = r.nextInt(17) + 1;
							Color c1 = getColor(r1i);
							Color c2 = getColor(r2i);

							FireworkEffect effect = FireworkEffect.builder()
									.flicker(r.nextBoolean()).withColor(c1)
									.withFade(c2).with(type)
									.trail(r.nextBoolean()).build();
							fwm.addEffect(effect);
							int rp = r.nextInt(2) + 1;
							fwm.setPower(rp);
							fw.setFireworkMeta(fwm);
							fw.setVelocity(fw.getVelocity()
									.multiply(1));
						}
					} else if (team.equalsIgnoreCase("blue")) {
						for (OfflinePlayer p2 : blue.getPlayers()) {
							Player p1 = (Player) p2;
							Firework fw = (Firework) p1.getWorld().spawnEntity(
									p1.getLocation(), EntityType.FIREWORK);
							FireworkMeta fwm = fw.getFireworkMeta();
							Random r = new Random();
							int rt = r.nextInt(5) + 1;
							Type type = Type.BALL;
							if (rt == 1)
								type = Type.BALL;
							if (rt == 2)
								type = Type.BALL_LARGE;
							if (rt == 3)
								type = Type.BURST;
							if (rt == 4)
								type = Type.CREEPER;
							if (rt == 5)
								type = Type.STAR;

							int r1i = r.nextInt(17) + 1;
							int r2i = r.nextInt(17) + 1;
							Color c1 = getColor(r1i);
							Color c2 = getColor(r2i);

							FireworkEffect effect = FireworkEffect.builder()
									.flicker(r.nextBoolean()).withColor(c1)
									.withFade(c2).with(type)
									.trail(r.nextBoolean()).build();
							fwm.addEffect(effect);
							int rp = r.nextInt(2) + 1;
							fwm.setPower(rp);
							fw.setFireworkMeta(fwm);
							fw.setVelocity(fw.getVelocity()
									.multiply(1));
						}
					}
				}
			}
		}, 0L, 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers())
					player.sendMessage(blitz.getPrefix() + "§bServer restarting in §35 §bseconds.");
			}
		}, 5 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers())
					player.sendMessage(blitz.getPrefix() + "§bServer restarting in §34 §bseconds.");
			}
		}, 6 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers())
					player.sendMessage(blitz.getPrefix() + "§bServer restarting in §33 §bseconds.");
			}
		}, 7 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers())
					player.sendMessage(blitz.getPrefix() + "§bServer restarting in §32 §bseconds.");
			}
		}, 8 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers())
					player.sendMessage(blitz.getPrefix() + "§bServer restarting in §31 §bsecond.");
			}
		}, 9 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					try {
						ByteArrayOutputStream b = new ByteArrayOutputStream();
						DataOutputStream out = new DataOutputStream(b);
						out.writeUTF("Connect");
						out.writeUTF("hub");
						player.sendPluginMessage(blitz, "BungeeCord",
								b.toByteArray());
					} catch (Exception ex) {
						System.out
								.println("Unable to send player to hub/lobby server.");
						player.kickPlayer(blitz.getPrefix() + "§cServer is restarting...");
					}
				}
			}
		}, 10 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers())
					player.kickPlayer(blitz.getPrefix() + "§cServer is restarting...");
			}
		}, 11 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				for (World w : blitz.getWorlds()) {
					arenaman.resetArena(w.getName());
				}
			}
		}, 12 * 20L);
		Bukkit.getScheduler().scheduleSyncDelayedTask(blitz, new Runnable() {
			public void run() {
				Bukkit.shutdown();
			}
		}, 20 * 20L);
	}

	public void addRedPoint() {
		red_score++;
		updateScoreboardPoints();
	}

	public void addBluePoint() {
		blue_score++;
		updateScoreboardPoints();
	}

	public void updateScoreboard() {
		if (GAMESTATE == LOBBY) {
			if (Bukkit.getOnlinePlayers().length < 10) {
				Objective obj = blitz.board.getObjective("lobby");
				obj.setDisplaySlot(DisplaySlot.SIDEBAR);
				obj.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Pre-Game");
				Score time = obj.getScore(ChatColor.YELLOW + "Needed:");
				time.setScore(10 - Bukkit.getOnlinePlayers().length);
				Score players = obj.getScore(ChatColor.AQUA + "Waiting:");
				players.setScore(Bukkit.getOnlinePlayers().length);
				for (Player p : Bukkit.getOnlinePlayers()) p.setScoreboard(blitz.board);
			} else if (Bukkit.getOnlinePlayers().length >= 10) {
				Objective obj = blitz.board.getObjective("pregame");
				obj.setDisplaySlot(DisplaySlot.SIDEBAR);
				obj.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Pre-Game");
				Score time = obj.getScore(ChatColor.YELLOW + "Time left:");
				time.setScore(lobby_countdown);
				Score players = obj.getScore(ChatColor.AQUA + "Waiting:");
				players.setScore(Bukkit.getOnlinePlayers().length);
				for (Player p : Bukkit.getOnlinePlayers()) p.setScoreboard(blitz.board);
			}
		} else if (GAMESTATE == GAME) {
			Objective obj = blitz.board.getObjective("game");
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);
			obj.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD
					+ "Blitz Stats");
			Score rp = obj.getScore(ChatColor.RED
					+ "Red Points:");
			rp.setScore(red_score);
			Score bp = obj.getScore(ChatColor.BLUE
					+ "Blue Points:");
			bp.setScore(blue_score);
			for (Player p : Bukkit.getOnlinePlayers()) p.setScoreboard(blitz.board);
		} else if (GAMESTATE == ENDING) {
			blitz.board.clearSlot(DisplaySlot.SIDEBAR);
			for (Player p : Bukkit.getOnlinePlayers()) p.setScoreboard(blitz.board);
		}
	}
	
	public void updateScoreboardLobby(Player p) {
		if (GAMESTATE == LOBBY) {
			if (Bukkit.getOnlinePlayers().length < 10) {
				Objective obj = blitz.board.getObjective(DisplaySlot.SIDEBAR);
				Score need = obj.getScore(ChatColor.YELLOW + "Needed:");
				need.setScore(10 - Bukkit.getOnlinePlayers().length);
				Score wait = obj.getScore(ChatColor.AQUA + "Waiting:");
				wait.setScore(Bukkit.getOnlinePlayers().length);
			} else if (Bukkit.getOnlinePlayers().length >= 10) {
				Objective obj = blitz.board.getObjective(DisplaySlot.SIDEBAR);
				Score need = obj.getScore(ChatColor.YELLOW + "Time left:");
				need.setScore(lobby_countdown);
				Score wait = obj.getScore(ChatColor.AQUA + "Waiting:");
				wait.setScore(Bukkit.getOnlinePlayers().length);
			}
		}
	}

	public void updateScoreboardPoints() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			Score rp = p
					.getScoreboard()
					.getObjective(DisplaySlot.SIDEBAR)
					.getScore(
							ChatColor.RED
									+ "Red Points:");
			rp.setScore(red_score);
			Score bp = p
					.getScoreboard()
					.getObjective(DisplaySlot.SIDEBAR)
					.getScore(
							ChatColor.BLUE
									+ "Blue Points:");
			bp.setScore(blue_score);
		}
	}

	public void giveKit(Player p) {
		if (p.hasPermission("blitz.vip")) {
			PlayerInventory pi = p.getInventory();
			pi.addItem(new ItemStack(Material.DIAMOND_SWORD, 1));
			ItemStack bow = new ItemStack(Material.BOW, 1);
			bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
			bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
			pi.addItem(bow);
			ItemStack dp = new ItemStack(Material.DIAMOND_PICKAXE, 1);
			dp.addEnchantment(Enchantment.DIG_SPEED, 1);
			pi.addItem(dp);
			pi.addItem(new ItemStack(Material.GOLDEN_APPLE, 3, (short) 1));
			pi.addItem(new ItemStack(Material.COOKED_BEEF, 10));
			pi.addItem(new ItemStack(Material.STONE, 64));
			pi.addItem(new ItemStack(Material.GLASS, 64));
			pi.addItem(new ItemStack(Material.LOG, 64));
			ItemStack shop = new ItemStack(Material.NETHER_STAR, 1);
			ItemMeta shopmeta = shop.getItemMeta();
			shopmeta.setDisplayName("§6BlitzPvP Shop §7(Right Click to Use)");
			shopmeta.setLore(Arrays.asList("§eUse this shop to purchase items that will be useful in-game."));
			shop.setItemMeta(shopmeta);
			pi.addItem(shop);
			pi.addItem(new ItemStack(Material.ARROW, 1));
			pi.setHelmet(new ItemStack(Material.IRON_HELMET, 1));
			pi.setChestplate(new ItemStack(Material.IRON_CHESTPLATE, 1));
			pi.setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
			pi.setBoots(new ItemStack(Material.IRON_BOOTS, 1));
		} else {
			PlayerInventory pi = p.getInventory();
			pi.addItem(new ItemStack(Material.IRON_SWORD, 1));
			ItemStack bow = new ItemStack(Material.BOW, 1);
			bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
			bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
			pi.addItem(bow);
			ItemStack dp = new ItemStack(Material.DIAMOND_PICKAXE, 1);
			dp.addEnchantment(Enchantment.DIG_SPEED, 1);
			pi.addItem(dp);
			pi.addItem(new ItemStack(Material.GOLDEN_APPLE, 3));
			pi.addItem(new ItemStack(Material.COOKED_BEEF, 10));
			pi.addItem(new ItemStack(Material.STONE, 64));
			pi.addItem(new ItemStack(Material.GLASS, 64));
			pi.addItem(new ItemStack(Material.LOG, 64));
			ItemStack shop = new ItemStack(Material.NETHER_STAR, 1);
			ItemMeta shopmeta = shop.getItemMeta();
			shopmeta.setDisplayName("§6BlitzPvP Shop §7(Right Click to Use)");
			shopmeta.setLore(Arrays.asList("§eUse this shop to purchase items that will be useful in-game."));
			shop.setItemMeta(shopmeta);
			pi.addItem(shop);
			pi.addItem(new ItemStack(Material.ARROW, 1));
			if (red.hasPlayer(p)) {
				ItemStack helm = new ItemStack(Material.LEATHER_HELMET, 1);
				LeatherArmorMeta helmmeta = (LeatherArmorMeta) helm
						.getItemMeta();
				helmmeta.setColor(Color.RED);
				helm.setItemMeta(helmmeta);
				ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
				LeatherArmorMeta chestmeta = (LeatherArmorMeta) chest
						.getItemMeta();
				chestmeta.setColor(Color.RED);
				chest.setItemMeta(chestmeta);
				ItemStack legs = new ItemStack(Material.LEATHER_LEGGINGS, 1);
				LeatherArmorMeta legsmeta = (LeatherArmorMeta) legs
						.getItemMeta();
				legsmeta.setColor(Color.RED);
				legs.setItemMeta(legsmeta);
				ItemStack boots = new ItemStack(Material.LEATHER_HELMET, 1);
				LeatherArmorMeta bootsmeta = (LeatherArmorMeta) boots
						.getItemMeta();
				bootsmeta.setColor(Color.RED);
				boots.setItemMeta(bootsmeta);
				pi.setHelmet(helm);
				pi.setChestplate(chest);
				pi.setLeggings(legs);
				pi.setBoots(boots);
			} else if (blue.hasPlayer(p)) {
				ItemStack helm = new ItemStack(Material.LEATHER_HELMET, 1);
				LeatherArmorMeta helmmeta = (LeatherArmorMeta) helm
						.getItemMeta();
				helmmeta.setColor(Color.BLUE);
				helm.setItemMeta(helmmeta);
				ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
				LeatherArmorMeta chestmeta = (LeatherArmorMeta) chest
						.getItemMeta();
				chestmeta.setColor(Color.BLUE);
				chest.setItemMeta(chestmeta);
				ItemStack legs = new ItemStack(Material.LEATHER_LEGGINGS, 1);
				LeatherArmorMeta legsmeta = (LeatherArmorMeta) legs
						.getItemMeta();
				legsmeta.setColor(Color.BLUE);
				legs.setItemMeta(legsmeta);
				ItemStack boots = new ItemStack(Material.LEATHER_HELMET, 1);
				LeatherArmorMeta bootsmeta = (LeatherArmorMeta) boots
						.getItemMeta();
				bootsmeta.setColor(Color.BLUE);
				boots.setItemMeta(bootsmeta);
				pi.setHelmet(helm);
				pi.setChestplate(chest);
				pi.setLeggings(legs);
				pi.setBoots(boots);
			}
		}
	}

	public String getTeam(Player p) {
		String team = "";
		if (red.hasPlayer(p)) {
			team = "§cRed Team";
		} else if (blue.hasPlayer(p)) {
			team = "§9Blue Team";
		}
		return team;
	}

	public void removeArmor(Player player) {
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);
	}

	private Color getColor(int i) {
		Color c = Color.AQUA;

		if (i == 1) {
			c = Color.AQUA;
		}
		if (i == 2) {
			c = Color.BLACK;
		}
		if (i == 3) {
			c = Color.BLUE;
		}
		if (i == 4) {
			c = Color.FUCHSIA;
		}
		if (i == 5) {
			c = Color.GRAY;
		}
		if (i == 6) {
			c = Color.GREEN;
		}
		if (i == 7) {
			c = Color.LIME;
		}
		if (i == 8) {
			c = Color.MAROON;
		}
		if (i == 9) {
			c = Color.NAVY;
		}
		if (i == 10) {
			c = Color.OLIVE;
		}
		if (i == 11) {
			c = Color.ORANGE;
		}
		if (i == 12) {
			c = Color.PURPLE;
		}
		if (i == 13) {
			c = Color.RED;
		}
		if (i == 14) {
			c = Color.SILVER;
		}
		if (i == 15) {
			c = Color.TEAL;
		}
		if (i == 16) {
			c = Color.WHITE;
		}
		if (i == 17) {
			c = Color.YELLOW;
		}

		return c;
	}

	public Location getBlueCore(World w) {
		String core = blitz.getConfig().getString(
				"worlds." + w.getName() + ".cores.blue");
		String[] coreloc = core.split(",");
		double x = Double.parseDouble(coreloc[0]);
		double y = Double.parseDouble(coreloc[1]);
		double z = Double.parseDouble(coreloc[2]);
		Location loc = new Location(w, x, y, z);
		return loc;
	}

	public Location getRedCore(World w) {
		String core = blitz.getConfig().getString(
				"worlds." + w.getName() + ".cores.red");
		String[] coreloc = core.split(",");
		double x = Double.parseDouble(coreloc[0]);
		double y = Double.parseDouble(coreloc[1]);
		double z = Double.parseDouble(coreloc[2]);
		Location loc = new Location(w, x, y, z);
		return loc;
	}

	public boolean isBroken(Location loc) {
		if (loc.getBlock().getType() == Material.AIR) {
			return true;
		}
		return false;
	}

	public void setBar(Player p, boolean red, boolean blue) {
		if (BarAPI.hasBar(p)) BarAPI.removeBar(p);
		if (blue && red)
			BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&',
					"&9Blue Core: &b✔    &6&lTeam Cores    &cRed Core: &b✔"));
		if (blue && !red)
			if (core_regen.containsKey(getRedCore(current_world)))
				BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&',
						"&9Blue Core: &b✔    &6&lTeam Cores    &cRed Core: &b" + TimeUtils.getTime(core_regen.get(getRedCore(current_world)))));
		if (!blue && red)
			BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&',
					"&9Blue Core: &b" + TimeUtils.getTime(core_regen.get(getBlueCore(current_world))) + "    &6&lTeam Cores    &cRed Core: &b✔"));
		if (!blue && !red)
			BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&',
					"&9Blue Core: &b" + TimeUtils.getTime(core_regen.get(getBlueCore(current_world))) + "    &6&lTeam Cores    &cRed Core: &b" + TimeUtils.getTime(core_regen.get(getRedCore(current_world)))));
	}
	
	public void openShop(Player p) {
		
		ItemStack stone = new ItemStack(Material.STONE, 64);
		ItemMeta stonemeta = stone.getItemMeta();
		List<String> stonelore = new ArrayList<String>();
		stonelore.add("§61 Stack of Stone");
		stonelore.add("§4§m--------------");
		stonelore.add("§3Price: §b25 Tokens");
		stonemeta.setLore(stonelore);
		stone.setItemMeta(stonemeta);
		
		ItemStack bread = new ItemStack(Material.BREAD, 1);
		ItemMeta breadmeta = bread.getItemMeta();
		List<String> breadlore = new ArrayList<String>();
		breadlore.add("§61 Bread");
		breadlore.add("§4§m--------------");
		breadlore.add("§3Price: §b10 Tokens");
		breadmeta.setLore(breadlore);
		bread.setItemMeta(breadmeta);
		
		ItemStack goldenapple = new ItemStack(Material.GOLDEN_APPLE, 1);
		ItemMeta gameta = goldenapple.getItemMeta();
		List<String> galore = new ArrayList<String>();
		galore.add("§61 Golden Apple");
		galore.add("§4§m--------------");
		galore.add("§3Price: §b50 Tokens");
		gameta.setLore(galore);
		goldenapple.setItemMeta(gameta);
		
		ItemStack goldenapple1 = new ItemStack(Material.GOLDEN_APPLE, 1, (short)1);
		ItemMeta gameta1 = goldenapple1.getItemMeta();
		List<String> galore1 = new ArrayList<String>();
		galore1.add("§61 God Apple");
		galore1.add("§4§m---------------");
		galore1.add("§3Price: §b200 Tokens");
		gameta1.setLore(galore1);
		goldenapple1.setItemMeta(gameta1);
		
		ItemStack dsword = new ItemStack(Material.DIAMOND_SWORD, 1);
		ItemMeta dswordmeta = dsword.getItemMeta();
		List<String> dswordlore = new ArrayList<String>();
		dswordlore.add("§61 Diamond Sword");
		dswordlore.add("§4§m---------------");
		dswordlore.add("§3Price: §b250 Tokens");
		dswordmeta.setLore(dswordlore);
		dsword.setItemMeta(dswordmeta);
		
		ItemStack darm = new ItemStack(Material.DIAMOND_HELMET, 1);
		ItemMeta darmmeta = darm.getItemMeta();
		List<String> darmlore = new ArrayList<String>();
		darmlore.add("§61 Diamond Helmet");
		darmlore.add("§4§m---------------");
		darmlore.add("§3Price: §b150 Tokens");
		darmmeta.setLore(darmlore);
		darm.setItemMeta(darmmeta);
		
		ItemStack darm1 = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
		ItemMeta darmmeta1 = darm1.getItemMeta();
		List<String> darmlore1 = new ArrayList<String>();
		darmlore1.add("§61 Diamond Chestplate");
		darmlore1.add("§4§m-----------------");
		darmlore1.add("§3Price: §b150 Tokens");
		darmmeta1.setLore(darmlore1);
		darm1.setItemMeta(darmmeta1);
		
		ItemStack darm11 = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
		ItemMeta darmmeta11 = darm11.getItemMeta();
		List<String> darmlore11 = new ArrayList<String>();
		darmlore11.add("§61 Diamond Leggings");
		darmlore11.add("§4§m---------------");
		darmlore11.add("§3Price: §b150 Tokens");
		darmmeta11.setLore(darmlore11);
		darm11.setItemMeta(darmmeta11);
		
		ItemStack darm111 = new ItemStack(Material.DIAMOND_BOOTS, 1);
		ItemMeta darmmeta111 = darm111.getItemMeta();
		List<String> darmlore111 = new ArrayList<String>();
		darmlore111.add("§61 Diamond Boots");
		darmlore111.add("§4§m---------------");
		darmlore111.add("§3Price: §b150 Tokens");
		darmmeta111.setLore(darmlore111);
		darm111.setItemMeta(darmmeta111);
		
		ItemStack water = new ItemStack(Material.WATER, 1);
		ItemMeta watermeta = water.getItemMeta();
		List<String> waterlore = new ArrayList<String>();
		waterlore.add("§61 Water");
		waterlore.add("§4§m--------------");
		waterlore.add("§3Price: §b30 Tokens");
		watermeta.setLore(waterlore);
		water.setItemMeta(watermeta);
		
		ItemStack lava = new ItemStack(Material.LAVA, 1);
		ItemMeta lavameta = lava.getItemMeta();
		List<String> lavalore = new ArrayList<String>();
		lavalore.add("§61 Lava");
		lavalore.add("§4§m--------------");
		lavalore.add("§3Price: §b40 Tokens");
		lavameta.setLore(lavalore);
		lava.setItemMeta(lavameta);
		
		ItemStack egg = new ItemStack(Material.MONSTER_EGG, 1, (short)50);
		ItemMeta eggmeta = egg.getItemMeta();
		List<String> egglore = new ArrayList<String>();
		egglore.add("§61 Creeper Egg");
		egglore.add("§4§m---------------");
		egglore.add("§3Price: §b150 Tokens");
		eggmeta.setLore(egglore);
		egg.setItemMeta(eggmeta);
		
		ItemStack h2 = new ItemStack(Material.POTION, 1, (short)16421);
		ItemMeta h2meta = h2.getItemMeta();
		List<String> h2lore = new ArrayList<String>();
		h2lore.add("§61 Health Potion");
		h2lore.add("§4§m--------------");
		h2lore.add("§3Price: §b40 Tokens");
		h2meta.setDisplayName("Splash Potion of Healing");
		h2meta.setLore(h2lore);
		h2.setItemMeta(h2meta);
		
		ItemStack ps = new ItemStack(Material.POTION, 1, (short)16388);
		PotionMeta psmeta = (PotionMeta)ps.getItemMeta();
		List<String> pslore = new ArrayList<String>();
		pslore.add("§61 Poison Potion");
		pslore.add("§4§m--------------");
		pslore.add("§3Price: §b40 Tokens");
		PotionEffect effect = new PotionEffect(PotionEffectType.POISON, 10 * 20, 0);
		psmeta.addCustomEffect(effect, true);
		psmeta.setDisplayName("Splash Potion of Poison");
		psmeta.setLore(pslore);
		ps.setItemMeta(psmeta);
		
		/**
		 * 64 Stone - 25, bread - 10, golden apple - 50, OP golden apple - 200, dsword - 250,
		 * diamond armor - 150 each, water - 30, lava - 40, creeper egg - 150, health 2 splash potion - 40,
		 * poison splash potion - 40
		 */
		
		Inventory shop = Bukkit.createInventory(null, 27, "§1BlitzPvP Shop");
		shop.setItem(0, stone);
		shop.setItem(1, bread);
		shop.setItem(2, goldenapple);
		shop.setItem(3, goldenapple1);
		shop.setItem(4, dsword);
		shop.setItem(5, darm);
		shop.setItem(6, darm1);
		shop.setItem(7, darm11);
		shop.setItem(8, darm111);
		shop.setItem(11, water);
		shop.setItem(12, lava);
		shop.setItem(13, egg);
		shop.setItem(14, h2);
		shop.setItem(15, ps);
		shop.setItem(22, getTokensItem(p));
		p.openInventory(shop);
	}
	
	public ItemStack getTokensItem(Player p) {
		ItemStack bal = new ItemStack(Material.GOLD_BLOCK, 1);
		ItemMeta balmeta = bal.getItemMeta();
		List<String> ballore = new ArrayList<String>();
		ballore.add("§eYour Balance:");
		ballore.add("§b" + TokenAPI.getTokens(p.getUniqueId()));
		balmeta.setLore(ballore);
		balmeta.setDisplayName("§6Tokens");
		bal.setItemMeta(balmeta);
		return bal;
	}

}
